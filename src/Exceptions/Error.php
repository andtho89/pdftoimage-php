<?php
namespace Appkita\PDFtoImage\Exceptions;
use Exception;

class InvalidLayerMethod extends Exception {

}
class InvalidFormat extends Exception
{
}
class PageDoesNotExist extends Exception
{
}
class PdfDoesNotExist extends Exception
{
}